count = 0


def convert(x, y):
    global count
    if (x == y):
        pass
    else:
        if (2*x > y):
            count += 1
            convert(x, y+1)
        else:
            count += 1
            convert(2*x, y)
    return count


print(convert(1, 16))
