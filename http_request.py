import requests
from bs4 import BeautifulSoup

POST_LOGIN_URL = 'http://45.79.43.178/source_carts/wordpress/wp-login.php'
REQUEST_URL = 'http://45.79.43.178/source_carts/wordpress/wp-admin'

login_data = {
    'log': 'admin',
    'pwd': '123456aA',
    'wp-submit': 'Log In',
    'redirect_to': 'http://45.79.43.178/source_carts/wordpress/wp-admin',
    'testcookie': '1'
}

with requests.Session() as s:
    post = s.post(POST_LOGIN_URL, data=login_data)
    r = s.get(REQUEST_URL)
    soup = BeautifulSoup(r.content, 'html5lib')
    print(soup.find('span', attrs={'class': 'display-name'}).get_text())
    # print(r.content)
    # for c in r.cookies:
    #     print(c.name, c.value)

